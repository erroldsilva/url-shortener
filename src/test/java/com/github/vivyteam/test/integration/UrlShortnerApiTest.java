package com.github.vivyteam.test.integration;

import com.github.vivyteam.domain.ShortenedUrl;
import com.github.vivyteam.repository.ShortenedUrlRepositoryInterface;
import com.github.vivyteam.routes.UrlShortnerRouter;
import com.github.vivyteam.routes.UrlShortnerRoutingHandler;
import com.github.vivyteam.service.UrlShortnerService;
import org.junit.jupiter.api.Test;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;

/**
 *
 * @author errol.dsilva
 */
@WebFluxTest
@ContextConfiguration(classes = {UrlShortnerRouter.class, UrlShortnerRoutingHandler.class, UrlShortnerService.class})
public class UrlShortnerApiTest {

    @MockBean
    private ShortenedUrlRepositoryInterface repository;

    @Autowired
    private WebTestClient webTestClient;

    @Test
    public void shouldReturnShortUrlRetrivedFromCacheWhenAvailable() {
        ShortenedUrl result = mockShortnedUrl();
        doReturn(Mono.just(result)).when(repository).get(any());
        this.webTestClient.get().uri(uriBuilder
                -> uriBuilder.path("/v1/shortner/shorten")
                        .queryParam("url", "http://google.com").build()).exchange()
                .expectStatus()
                .isCreated()
                .expectBody()
                .jsonPath("url").isEqualTo(result.getUrl())
                .jsonPath("shortUrl").isEqualTo(result.getShortUrl());
        verify(repository, never()).save(any());
    }

    @Test
    public void shouldSaveAndReturnShortUrlInCacheWhenNotAvailable() {
        ShortenedUrl result = mockShortnedUrl();
        doReturn(Mono.empty()).when(repository).get(any());
        doReturn(Mono.just(result)).when(repository).save(any());
        this.webTestClient.get().uri(uriBuilder
                -> uriBuilder.path("/v1/shortner/shorten")
                        .queryParam("url", "http://google.com").build()).exchange()
                .expectStatus()
                .isCreated()
                .expectBody()
                .jsonPath("url").isEqualTo(result.getUrl())
                .jsonPath("shortUrl").isEqualTo(result.getShortUrl());
        verify(repository).save(any());
    }

    @Test
    public void shouldReturnErrorWhenLongUrlIsNotPassed() {
        this.webTestClient.get().uri(uriBuilder
                -> uriBuilder.path("/v1/shortner/shorten").build()).exchange()
                .expectStatus().isNotFound();
    }

    @Test
    public void shouldReturnErrorWhenBadUrlIsNotPassed() {
        this.webTestClient.get().uri(uriBuilder
                -> uriBuilder.path("/v1/shortner/shorten")
                        .queryParam("url", "xxx").build()).exchange()
                .expectStatus()
                .isNotFound();
    }

    @Test
    public void shouldReturnOrginalUrlWhenShortUrlIsPassed() {
        ShortenedUrl result = mockShortnedUrl();
        doReturn(Mono.just(result)).when(repository).get(any());
        this.webTestClient.get().uri(uriBuilder
                -> uriBuilder.path("/v1/shortner/expand")
                        .queryParam("url", "http://google.com").build()).exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .jsonPath("url").isEqualTo(result.getUrl())
                .jsonPath("shortUrl").isEqualTo(result.getShortUrl());
    }

    @Test
    public void shouldThrowErrorWhenNoHashMatchingShortnedUrlIsFound() {
        doReturn(Mono.empty()).when(repository).get(any());
        this.webTestClient.get().uri(uriBuilder
                -> uriBuilder.path("/v1/shortner/expand")
                        .queryParam("url", "http://google.com").build()).exchange()
                .expectStatus()
                .is5xxServerError();
    }

    @Test
    public void shouldRedirectWhenShortnedUrlIsCalled() {
        ShortenedUrl result = mockShortnedUrl();
        doReturn(Mono.just(result)).when(repository).get(any());
        this.webTestClient.get().uri(uriBuilder
                -> uriBuilder.path("/hash").build()).exchange()
                .expectStatus()
                .isPermanentRedirect();
    }

    private ShortenedUrl mockShortnedUrl() {
        return ShortenedUrl.builder()
                .shortUrl("http://url/short")
                .url("http://url-long.com")
                .urlHash("short")
                .build();
    }

}

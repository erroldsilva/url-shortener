package com.github.vivyteam.routes;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RequestPredicates;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.web.reactive.function.server.RequestPredicates.queryParam;
import org.springframework.web.reactive.function.server.RouterFunctions;

/**
 *
 * @author errol.dsilva
 */
@Configuration
public class UrlShortnerRouter {

    String regexUrl = "^(https?)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";

    @Bean
    public RouterFunction<ServerResponse> routes(UrlShortnerRoutingHandler controller) {
        return RouterFunctions.route().GET("/v1/shortner/expand",
                RequestPredicates.all()
                        .and(queryParam(
                                "url", t ->  t.matches(regexUrl))), controller::expand)
                .GET("/v1/shortner/shorten",
                        RequestPredicates.all()
                                .and(queryParam("url", t-> t.matches(regexUrl))), controller::shorten)
                .GET("/{hash}", controller::redirect)
                .build();
    }
}

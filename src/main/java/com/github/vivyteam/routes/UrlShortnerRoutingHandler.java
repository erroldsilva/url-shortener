package com.github.vivyteam.routes;

import com.github.vivyteam.domain.ShortenedUrl;
import com.github.vivyteam.domain.dto.ShortenedUrlResponseDto;
import com.github.vivyteam.service.UrlShortnerServiceInterface;
import java.net.URI;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;
import static org.springframework.web.reactive.function.server.ServerResponse.status;

/**
 *
 * @author errol.dsilva
 */
@Slf4j
@RequiredArgsConstructor
@Component

public class UrlShortnerRoutingHandler {

    @Autowired
    private final UrlShortnerServiceInterface urlShortnerService;

    public Mono<ServerResponse> shorten(ServerRequest serverRequest) {
        final String url = serverRequest.queryParam("url").get();
        log.info("Shortening url {} ", url);
        return urlShortnerService.shorten(url)
                .doOnNext(t -> log.info("Created hash {} for {}", t.getUrlHash(), t.getUrl()))
                .flatMap(t -> status(HttpStatus.CREATED)
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(toResponseDto(t)));

    }

    public Mono<ServerResponse> expand(ServerRequest serverRequest) {
        final String url = serverRequest.queryParam("url").get();
        return urlShortnerService
                .expand(url)
                .doOnNext(details -> log.info("Found in cache {}", details))
                .flatMap(t -> status(HttpStatus.OK).bodyValue(toResponseDto(t)))
                .switchIfEmpty(
                        status(HttpStatus.INTERNAL_SERVER_ERROR)
                                .bodyValue("Unexpected error occurred while generating URL.")
                );
    }

    public Mono<ServerResponse> redirect(ServerRequest serverRequest) {
        final String hash = serverRequest.pathVariable("hash");
        log.info("HASH {}", hash);
        return urlShortnerService.expand(hash)
                .doOnNext(t -> log.info("Will redirect to {}", t.getUrl()))
                .flatMap(t -> ServerResponse.permanentRedirect(URI.create(t.getUrl())).build())
                .switchIfEmpty(
                        status(HttpStatus.INTERNAL_SERVER_ERROR)
                                .bodyValue("Unexpected error occurred while generating URL.")
                );

    }

    private ShortenedUrlResponseDto toResponseDto(ShortenedUrl shortnedUrl) {
        return ShortenedUrlResponseDto
                .builder()
                .shortUrl(shortnedUrl.getShortUrl())
                .url(shortnedUrl.getUrl())
                .build();
    }

}

package com.github.vivyteam.repository;

import java.time.Duration;
import javax.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;

import org.springframework.data.redis.core.ReactiveRedisOperations;
import org.springframework.stereotype.Component;

import reactor.core.publisher.Mono;

/**
 *
 * @author errol.dsilva
 */

@Component
@RequiredArgsConstructor
public class ReactiveRedis {

    private final ReactiveRedisOperations<String, Object> redisOperations;

    public Mono<Boolean> set(@NotNull String key, @NotNull Object val, @NotNull long cacheTtl) {
        return redisOperations.opsForValue()
                .setIfAbsent(
                        key,
                        val,
                        Duration.ofMinutes(cacheTtl)
                );
    }

    public Mono<Object> get(@NotNull String key, @NotNull long cacheTtl) {
        return redisOperations
                .opsForValue()
                .getAndExpire(
                        key,
                        Duration.ofMinutes(cacheTtl)
                );
    }

}

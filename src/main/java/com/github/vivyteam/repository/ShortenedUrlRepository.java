package com.github.vivyteam.repository;

import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.vivyteam.domain.ShortenedUrl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

/**
 *
 * @author errol.dsilva
 */
@Slf4j
@Repository
@RequiredArgsConstructor
public class ShortenedUrlRepository implements ShortenedUrlRepositoryInterface {

    private final int cacheTtl = 10;
     
    private final ObjectMapper mapper;

    private final ReactiveRedis reactiveRedis;

    @Override
    public Mono<ShortenedUrl> save(ShortenedUrl shortenedUrl) {
        return reactiveRedis
                .set(shortenedUrl.getUrlHash(), shortenedUrl, cacheTtl)
                .map(t -> shortenedUrl);

    }

    @Override
    public Mono<ShortenedUrl> get(String hash) {
        return reactiveRedis.get(hash, cacheTtl)
                .doOnNext(s -> log.info("Found {} ", s))
                .flatMap(s -> 
                        Mono.justOrEmpty(mapper.convertValue(s, ShortenedUrl.class))
                );
    }
}


package com.github.vivyteam.repository;

import com.github.vivyteam.domain.ShortenedUrl;

import reactor.core.publisher.Mono;

/**
 *
 * @author errol.dsilva
 */
public interface ShortenedUrlRepositoryInterface {

    /**
     *
     * @param shortenedUrl
     * @return
     */
    Mono<ShortenedUrl> save(ShortenedUrl shortenedUrl);

    /**
     *
     * @param hash
     * @return
     */
    Mono<ShortenedUrl> get(String hash);
   
}

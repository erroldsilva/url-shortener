package com.github.vivyteam.service;

import java.nio.charset.StandardCharsets;

import com.github.vivyteam.domain.ShortenedUrl;
import com.github.vivyteam.repository.ShortenedUrlRepositoryInterface;
import com.google.common.hash.Hashing;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import reactor.core.publisher.Mono;

/**
 *
 * @author errol.dsilva
 */
@Slf4j
@RequiredArgsConstructor
@Service
public class UrlShortnerService implements UrlShortnerServiceInterface {

    @Value("${application.domainPrefix}")
    private String domainPrefix;

    @Autowired
    private final ShortenedUrlRepositoryInterface shortenedUrlRepository;

    /**
     *
     * @param url
     * @return
     */
    @Override
    public final Mono<ShortenedUrl> shorten(final String url) {
        final String hash = Hashing.murmur3_32().hashString(url, StandardCharsets.UTF_8).toString();
        return findInCache(hash)
                .flatMap(t -> Mono.just(t))
                .switchIfEmpty(
                        Mono.just(
                                ShortenedUrl.builder()
                                        .url(url)
                                        .urlHash(hash)
                                        .shortUrl(addDomain(hash)).build()
                                )
                                .doOnNext(t -> log.info("Will cache {} as it was not found", t.toString()))
                                .flatMap(shortenedUrlRepository::save)
                );

    }

    /**
     *
     * @param url
     * @return
     */
    @Override
    public final Mono<ShortenedUrl> expand(final String hashedUrl) {
        return findInCache(getHashFromUrl(hashedUrl));
    }

    private Mono<ShortenedUrl> findInCache(final String hash) {
        log.info("Finding {} in cache", hash);
        return shortenedUrlRepository.get(hash);
    }

    private String getHashFromUrl(final String url) {
        return url.replaceFirst(".*/([^/?]+).*", "$1");
    }

    private String addDomain(String urlHash) {
        log.info("adding doamin to {}", urlHash);
        return String.join("/", domainPrefix, urlHash);
    }

}

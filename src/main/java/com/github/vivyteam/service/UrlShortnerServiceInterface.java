package com.github.vivyteam.service;

import com.github.vivyteam.domain.ShortenedUrl;

import reactor.core.publisher.Mono;

/**
 *
 * @author errol.dsilva
 */
public interface UrlShortnerServiceInterface {
	
    /**
     *
     * @param url
     * @return
     */
    public Mono<ShortenedUrl> shorten(String url);
	
    /**
     *
     * @param hashedUrl
     * @return
     */
    public Mono<ShortenedUrl> expand(String hashedUrl);
	

}

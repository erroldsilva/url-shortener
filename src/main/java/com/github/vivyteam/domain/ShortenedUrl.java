package com.github.vivyteam.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import org.hibernate.validator.constraints.URL;
import org.springframework.data.annotation.Id;

import javax.validation.constraints.NotNull;

/**
 *
 * @author errol.dsilva
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ShortenedUrl {

    @NotNull
    @Id
    private String urlHash;

    @NotNull
    @URL
    private String url;
    
    @NotNull
    @URL
    private String shortUrl;


}

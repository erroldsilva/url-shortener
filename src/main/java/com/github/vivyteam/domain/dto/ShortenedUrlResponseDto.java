
package com.github.vivyteam.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author errol.dsilva
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ShortenedUrlResponseDto {
    private String shortUrl;
    private String url;
}
